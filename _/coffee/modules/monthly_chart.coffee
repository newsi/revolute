Currency.MonthlyChart = do ->
    CV =
        canvas: _doc.querySelector '#monthly_chart'
        averageMonth: $ '.average_month'
        maxMonth: $ '.max_month'
        monthpicker: $ '#monthpicker'
        selectedMonthName: $ '#selectedMonthName'
        selectedYear: $ '#selectedYear'

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        _win.chartMonth = new Chart(ctx).Line(data2, options)

    #setting Datepicker and writing month info in the "Selected month" panel
    initCalendar = ->
        CV.monthpicker.datepicker({
            maxDate: new Date(),
            minDate: new Date("2015-11"),
            changeMonth: false,
            nextText: "Next",
            prevText: "Previous",
            onChangeMonthYear: (year, month) ->
                CV.selectedYear.text(year)
                monthArr = []
                monthArr[1] = "January"
                monthArr[2] = "February"
                monthArr[3] = "March"
                monthArr[4] = "April"
                monthArr[5] = "May"
                monthArr[6] = "June"
                monthArr[7] = "July"
                monthArr[8] = "August"
                monthArr[9] = "September"
                monthArr[10] = "October"
                monthArr[11] = "November"
                monthArr[12] = "December"
                CV.selectedMonthName.text(monthArr[month])
                do scrollToInfo
                date = "#{year}-#{month}"
                setInfo(date)
        })

    #setting info
    setInfo = (date) ->
        $.each(data2.datasets, ->
            @data = []
        )
        getInfo(date)

    #scroll to "Selected month" panel
    scrollToInfo = ->
        h = $('.c__daily').height()
        $('html, body').animate({scrollTop: h}, 500)

    #get current month and writing it in "Selected month" panel
    getCurrentMonth = ->
        monthName = CV.monthpicker.datepicker("getDate")
        monthArr = []
        monthArr[1] = "January"
        monthArr[2] = "February"
        monthArr[3] = "March"
        monthArr[4] = "April"
        monthArr[5] = "May"
        monthArr[6] = "June"
        monthArr[7] = "July"
        monthArr[8] = "August"
        monthArr[9] = "September"
        monthArr[10] = "October"
        monthArr[11] = "November"
        monthArr[12] = "December"
        d = new Date()
        month = d.getMonth() + 1
        CV.selectedMonthName.text(monthArr[month])
        CV.selectedYear.text($.datepicker.formatDate('yy', monthName))

    #calculating the average rate of currency of the day
    average = ->
        sum = 0
        $.each(data.datasets[1].data, ->
            sum+=parseFloat(this) || 0
        );
        averageMonth = (sum / data.datasets[1].data.length).toFixed(2)
        #CV.averageMonth.text(averageMonth)

    #calculating the maximum rate of currency of the day
    max = ->
        maxArr = []
        $.each(data2.datasets,  ->
            maxFromMonth = Math.max.apply(Math, @.data).toFixed(2)
            maxArr.push(maxFromMonth)
        )
        maxMonth = Math.max.apply(Math, maxArr).toFixed(2)
        CV.maxMonth.text(maxMonth)

    #get info via Ajax and writing it into configs.coffee
    getInfo = (date) ->
        if !date
            monthName = new Date()
        else
            monthName = new Date(date)
        month = if monthName.getMonth() + 1 < 10 then '0' + (monthName.getMonth() + 1) else monthName.getMonth() + 1
        year = monthName.getFullYear()
        dateFinal = "#{year}-#{month}"
        ajax_call = ->
            $.ajax({
                url: "http://104.236.42.50/revolut/index.php",
                dataType: "json",
                method: "POST",
                data: {"period": "month", "date": "#{dateFinal}" },
            }).success( (response) ->
                n = 0
                sum = 0
                $.each(response, (key,value) ->
                    n++
                    sum += parseFloat(value.average) || 0
                    data2.datasets[0].data.push(value.max)
                    data2.datasets[1].data.push(value.average)
                    average = (sum / n).toFixed(2)
                    CV.averageMonth.text(average)
                    if _win.chartMonth
                        _win.chartMonth.destroy()
                )
            ).done( ->
                initCanvas()
                max()
            )
        ajax_call()


    init = ->
        do initCalendar
        do getCurrentMonth
        do getInfo

    return {
    init: init
    }