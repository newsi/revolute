Currency.DailyChart = do ->
    CV =
        canvas: _doc.querySelector '#daily_chart'
        currentRate: $ '.current'
        maxDay: $ '.max'
        dayName: $ '#dayName'
        selectedDayName: $ '#selectedDayName'
        dayInfo: $ '#dayInfo'
        selectedDayInfo: $ '#selectedDayInfo'
        calendar: $ '#datepicker'
        calendarLink: $ 'td[data-event="click"]'
        averageDay: $ ".average"

    #initialize canvas with daily charts
    initCanvas = ->
        ctx = CV.canvas.getContext "2d"
        _win.chart = new Chart(ctx).Bar(data, options)

    #initialize calendar
    initCalendar = ->
        CV.calendar.datepicker({
            maxDate: new Date(),
            minDate: new Date("2015-11-04"),
            onSelect: ->
                do showSelectedInfo
                do setInfo
                do scrollToInfo
            })

    #get current date and write it in header
    getCurrentDate = ->
        dayName = CV.calendar.datepicker("getDate")
        CV.dayName.text($.datepicker.formatDate('DD', dayName))
        CV.dayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))

    #get selected date, writing it in "Selected day" panel, clear old info and sent Ajax
    setInfo =  ->
        dayName = CV.calendar.datepicker("getDate")
        CV.selectedDayName.text($.datepicker.formatDate('DD', dayName))
        CV.selectedDayInfo.text($.datepicker.formatDate('dd.mm.yy', dayName))
        $.each(data.datasets, ->
            @data = []
        )
        getInfo()

    #scroll to "Selected day" panel when day in Datepicker is selected
    showSelectedInfo = ->
        $('.c__daily_selected_h').hide()
        $('.c__daily_selected .c__daily_header').show()
    scrollToInfo = ->
        h = $('.c__daily_wrap').height()
        $('html, body').animate({scrollTop: h}, 500)

    #calculating the average rate of the selected day
    average = ->
        sum = 0
        $.each(data.datasets, ->
            sum+=parseFloat(@data) || 0
        );
        averageDay = (sum / data.datasets[0].data.length).toFixed(2)
        CV.averageDay.text(averageDay)

    #sending Ajax and receiving the current rate
    current = ->
        $.ajax({
            url: "http://104.236.42.50/revolut/index.php",
            dataType: "json",
            method: "POST",
            data: {"period": "now"},
        }).success( (data) ->
            CV.currentRate.text(data.now)
        )

    #calculating the maximum rate of currency of the day
    max = ->
        maxArr = []
        $.each(data.datasets,  ->
            maxFromPeriod = Math.max.apply(Math, @.data).toFixed(2)
            maxArr.push(maxFromPeriod)
        )
        maxDay = Math.max.apply(Math, maxArr).toFixed(2)
        CV.maxDay.text(maxDay)

    #get info via Ajax and writing it into configs.coffee
    getInfo = ->
        day = CV.calendar.datepicker("getDate")
        date = $.datepicker.formatDate('yy-mm-dd', day)
        ajax_call = ->
            $.ajax({
                url: "http://104.236.42.50/revolut/index.php",
                method: "POST",
                dataType: "json",
                data: {"period": "day", "date": "#{date}"},
            }).success( (response) ->
                i = 0
                n = 0
                sum = 0
                $.each(response, (key, value) ->
                    i++
                    n++
                    if i >= 4
                        i = 0
                    data.datasets[i].data.push(value.currency)
                    sum += parseFloat(value.currency) || 0
                    average = (sum / n).toFixed(2)
                    CV.averageDay.text(average)
                    if _win.chart
                        _win.chart.destroy()
                )
            ).done( ->
                initCanvas()
                max()
            )
        ajax_call()

    init = ->
        do initCalendar
        do getInfo
        do current
        do getCurrentDate

    return {
        init: init
    }